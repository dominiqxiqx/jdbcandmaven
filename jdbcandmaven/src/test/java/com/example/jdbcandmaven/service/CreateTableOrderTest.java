package com.example.jdbcandmaven.service;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

public class CreateTableOrderTest {
	
	@Ignore
	@Test
	public void checkIfTableExistTest() {
		assertFalse(new CreateTableOrder().checkIfTableExist());
	}
	
	@Test
	public void checkIfTableExistAfterCreateSqlTest() {
		CreateTableOrder createTableOrder = new CreateTableOrder();
		createTableOrder.createTable();
		assertTrue(createTableOrder.checkIfTableExist());
	}
}
